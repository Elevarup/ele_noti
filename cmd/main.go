package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/Elevarup/ele_noti/internal/raspberry"
	"gitlab.com/Elevarup/ele_noti/internal/routers"
	"gitlab.com/Elevarup/ele_noti/pkg/commons"
)

func main() {
	// environment variables
	commons.LoadEnvironments()
	port := commons.Env.SystemPort
	log.Printf("server - port: %s\n\n", port)

	routerMux := mux.NewRouter()
	for _, r := range routers.Routers {
		log.Println("method: ", r.Method, "path: ", r.Path)
		routerMux.HandleFunc(r.Path, r.Handler).Methods(r.Method)
	}
	srv := &http.Server{
		Addr:    fmt.Sprintf("0.0.0.0:%s", port),
		Handler: routerMux,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()
	go raspberry.Rpio.ListenMotionSensor()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	srv.Shutdown(ctx)

	log.Println("shutting down")
	os.Exit(0)
}
