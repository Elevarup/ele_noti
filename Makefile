
all: build scp

build:
	env GOARCH=arm64 GOOS=linux go build -o bin/noti cmd/*.go
scp:
	scp bin/noti r@192.168.18.30:/home/r/Documents
	scp .env r@192.168.18.30:/home/r/Documents
