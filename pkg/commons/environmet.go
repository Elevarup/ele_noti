package commons

import (
	"log"

	"github.com/ilyakaznacheev/cleanenv"
)

type (
	environment struct {
		SystemPort string `env:"system_port" env-default:"9090"`

		//Sms - Twilio
		TwilioAccountSID  string `env:"twilio_account_sid"`
		TwilioAuthToken   string `env:"twilio_auth_token"`
		TwilioPhoneNumber string `env:"twili_phone_number"`

		//Email - Gmail
		EmailGmailSenderName    string `env:"email_gmail_sender_name"`
		EmailGmailSenderAddress string `env:"email_gmail_sender_address"`
		EmailGmailSenderPass    string `env:"email_gmail_sender_password"`
	}
)

var (
	Env environment
)

func LoadEnvironments() {
	err := cleanenv.ReadConfig(".env", &Env)
	if err != nil {
		log.Fatal("load environment, error: ", err)
	}
}
