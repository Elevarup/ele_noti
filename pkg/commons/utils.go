package commons

const (
	SmtpAuthAddressGmail = "smtp.gmail.com"
	SmtpServerAddress    = "smtp.gmail.com:587"
)

// SMS - FACTORY
type ProviderSms uint8

const (
	ProviderSmsTwilio ProviderSms = iota
	ProviderSmsGoogle
)
