package twiliosms

import (
	"encoding/json"
	"log"

	"github.com/twilio/twilio-go"
	twilioApi "github.com/twilio/twilio-go/rest/api/v2010"
	"gitlab.com/Elevarup/ele_noti/pkg/commons"
)

type (
	TwilioSms struct{}
)

func (t *TwilioSms) Send(to, message string) (err error) {
	// TODO: validar que las credenciales tengan valores

	client := twilio.NewRestClientWithParams(twilio.ClientParams{
		Username: commons.Env.TwilioAccountSID,
		Password: commons.Env.TwilioAuthToken,
	})

	params := &twilioApi.CreateMessageParams{}
	params.SetFrom(commons.Env.TwilioPhoneNumber)

	params.SetTo(to)
	params.SetBody(message)

	resp, err := client.Api.CreateMessage(params)
	if err == nil {
		b, _ := json.Marshal(resp)
		log.Println("Response: ", string(b))
	}
	return
}
