package smsfactory

import (
	"errors"

	twiliosms "gitlab.com/Elevarup/ele_noti/internal/factory/sms_factory/twilio_sms"
	"gitlab.com/Elevarup/ele_noti/pkg/commons"
)

type (
	SmsProvider interface {
		Send(to, message string) error
	}
)

func New(p commons.ProviderSms) (sProvider SmsProvider, err error) {

	switch p {
	// twilio
	case commons.ProviderSmsTwilio:
		sProvider = &twiliosms.TwilioSms{}
		err = nil
	default:
		err = errors.New("provider sms not found")
	}

	return
}
