package middlewars

import (
	"log"
	"net/http"
)

func BlaMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("log ---> BlaMiddleware")
		next.ServeHTTP(w, r)
	}
}

func BleMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("log --->>>  BLE BLE BLE")
		next.ServeHTTP(w, r)
	}
}

func PanicRecoveryMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {

			if err := recover(); err != nil {
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				log.Println("log -->> recovery panic, defer")
			}
		}()

		next.ServeHTTP(w, r)
	}
}
