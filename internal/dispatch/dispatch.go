package dispatch

type (
	Dispatch struct {
		Listener chan func()
	}
)

// Add - add subscriber
func (d *Dispatch) Add(f func()) {
	d.Listener <- f
}

// Run - exec suscriber
func (d *Dispatch) Run() {
	for {
		select {
		case f := <-d.Listener:
			d.run(f)
		}
	}
}

func (d *Dispatch) run(f func()) {
	go f()
}
