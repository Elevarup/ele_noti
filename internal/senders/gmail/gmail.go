package gmail

import (
	"fmt"
	"net/smtp"

	"github.com/jordan-wright/email"
	"gitlab.com/Elevarup/ele_noti/internal/senders"
	"gitlab.com/Elevarup/ele_noti/pkg/commons"
)

type (
	Gmail struct {
		name              string
		fronEmailAddress  string
		fromEmailPassword string
	}
)

func New(name, fromEmailAddress, fromEmailPassword string) senders.EmailSender {
	return &Gmail{name, fromEmailAddress, fromEmailPassword}
}

func (g *Gmail) Send(
	subject string,
	content string,
	to []string,
	cc []string,
	bcc []string,
	attachFiles []string,
) error {

	e := email.NewEmail()

	e.From = fmt.Sprintf("%s <%s>", g.name, g.fronEmailAddress)
	e.Subject = subject
	e.HTML = []byte(content)
	e.To = to
	e.Cc = cc
	e.Bcc = bcc

	for _, f := range attachFiles {
		_, err := e.AttachFile(f)
		if err != nil {
			return fmt.Errorf("failder to attach file %s: %w", f, err)
		}
	}

	smtAuth := smtp.PlainAuth("", g.fronEmailAddress, g.fromEmailPassword, commons.SmtpAuthAddressGmail)

	return e.Send(commons.SmtpServerAddress, smtAuth)
}
