package gmail

import (
	"fmt"
	"strings"
	"testing"

	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/Elevarup/ele_noti/pkg/commons"
)

const (
	pathAbsolute = "./../../.."
)

// MOCK
type (
	MockSender struct{}
)

func (m *MockSender) Send(subject string, content string, to []string, cc []string, bcc []string, attachFiles []string) error {
	return nil
}

func TestSendWithGmail(t *testing.T) {
	path := func(s string) string { return fmt.Sprintf("%s/%s", pathAbsolute, s) }
	stringEmptyFunc := func(s string) bool { return strings.Trim(s, " ") == "" }

	err := cleanenv.ReadConfig(path(".env"), &commons.Env)
	if err != nil {
		t.Errorf("file .env not found, error: %v", err)
	}

	name := commons.Env.EmailGmailSenderName
	if stringEmptyFunc(name) {
		t.Errorf("environment email_gmail_sender_name is empty")
	}
	address := commons.Env.EmailGmailSenderAddress
	if stringEmptyFunc(address) {
		t.Errorf("environment email_gmail_sender_address is empty")
	}
	password := commons.Env.EmailGmailSenderPass
	if stringEmptyFunc(password) {
		t.Errorf("environment email_gmail_sender_password is empty")
	}

	sender := &MockSender{}

	subject := "A test email"
	content := `
    <h1>Hello Mr. English </h1>
    <p>How are your?</p>
    `

	to := []string{"test@test.com"}
	attachFiles := []string{path("test.jpeg")}

	err = sender.Send(subject, content, to, nil, nil, attachFiles)
	if err != nil {
		t.Errorf("error send email, error: %v\n", err)
	}
}
