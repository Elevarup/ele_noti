package senders

type (
	EmailSender interface {
		Send(
			subject string,
			content string,
			to []string,
			cc []string,
			bcc []string,
			attachFiles []string,
		) error
	}
)
