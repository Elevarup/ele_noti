package sms

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/Elevarup/ele_noti/internal/dispatch"
	smsfactory "gitlab.com/Elevarup/ele_noti/internal/factory/sms_factory"
	"gitlab.com/Elevarup/ele_noti/pkg/commons"
)

type (
	HandlerSms struct{ D *dispatch.Dispatch }
)

func (h *HandlerSms) BlaPanic(w http.ResponseWriter, r *http.Request) {
	panic("panic panic panic")
}

func (h *HandlerSms) Queries(w http.ResponseWriter, r *http.Request) {
	list := r.URL.Query()
	var builder strings.Builder

	for key, value := range list {
		builder.WriteString(fmt.Sprintf("key: %s, value: %v, size: %d\n", key, value, len(value)))
	}
	log.Println(builder.String())
	w.Write([]byte(builder.String()))
}

func (h *HandlerSms) Params(w http.ResponseWriter, r *http.Request) {
	list := mux.Vars(r)
	var builder strings.Builder

	for key, value := range list {
		builder.WriteString(fmt.Sprintf("key: %s, value: %v, size: %d\n", key, value, len(value)))
	}
	log.Println(builder.String())
	w.Write([]byte(builder.String()))
}
func (h *HandlerSms) Send(w http.ResponseWriter, r *http.Request) {

	to := r.URL.Query().Get("to")
	message := r.URL.Query().Get("sms")
	// TODO: validar que "to" y "sms", tengan valores

	if !strings.Contains(to, "+") {
		to = strings.Trim(to, " ")
		to = fmt.Sprintf("+%s", to)
	}
	//FIX: EN ESTE CASO NO ES PRÁCTICO USAR EL PATRON FACTORY, ANALIZARLO MEJOR
	providerSms, err := smsfactory.New(commons.ProviderSmsTwilio)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = providerSms.Send(to, message)
	if err != nil {
		log.Println("Error sending SMS message: " + err.Error())
		fmt.Fprintln(w, "Error sending SMS message: ", err.Error())
	} else {
		ok := struct {
			Code        int    `json:"code"`
			Description string `json:"description"`
		}{
			Code:        http.StatusOK,
			Description: fmt.Sprintf("ok"),
		}

		json.NewEncoder(w).Encode(ok)
	}

}
