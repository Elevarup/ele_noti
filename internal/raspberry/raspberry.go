package raspberry

import (
	"fmt"
	"log"
	"time"

	"github.com/stianeikeland/go-rpio/v4"
)

type (
	pio struct{}
)

var (
	Rpio pio = pio{}
	pin      = rpio.Pin(17)
)

func (r *pio) ListenMotionSensor() {

	// TODO: open gpio - raspberry_pi
	if err := rpio.Open(); err != nil {
		log.Println("rpio, error: ", err)
		return
	}
	// TODO: defer gpio
	defer rpio.Close()

	pin.Input()
	// TODO: cuánto es el tiempo espera
	// por ahora se va aprobar con 2 segundos
	timer := time.NewTicker(100 * time.Millisecond)
	// TODO: Listen sensor motion
	for {
		select {
		//case tick := <-timer.C:
		case <-timer.C:
			//case <- time
			t := pin.Read()
			//fmt.Println("timer select, result pin.Read(): ", t)
			if t == 1 {
				fmt.Println("moving")
			} else {
				fmt.Println("stop")
			}

		}
	}
	// TODO: conditional send sms or email
}
