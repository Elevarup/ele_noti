package routers

import (
	"net/http"

	"gitlab.com/Elevarup/ele_noti/internal/dispatch"
)

type (
	Router struct {
		Method  string
		Handler http.HandlerFunc
		Path    string
		Pipes   any
	}
)

var (
	Routers []Router
)

func init() {
	d := dispatch.Dispatch{Listener: make(chan func(), 3)}

	// cargando los recurso de mensaje de texto (sms)
	InitRouterSms(&d)

	go d.Run()
}
