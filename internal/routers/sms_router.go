package routers

import (
	"fmt"
	"net/http"

	"gitlab.com/Elevarup/ele_noti/internal/dispatch"
	"gitlab.com/Elevarup/ele_noti/internal/handlers/sms"
	"gitlab.com/Elevarup/ele_noti/internal/middlewars"
)

func InitRouterSms(d *dispatch.Dispatch) {
	handler := sms.HandlerSms{D: d}
	prefix := fmt.Sprint("sms")
	r := []Router{
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/%s", prefix),
			Handler: handler.Send,
			// TODO: crear la estructura para recibir los valores enviados en la consulta
			Pipes: "",
		},
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/%s/panic", prefix),
			Handler: middlewars.PanicRecoveryMiddleware(handler.BlaPanic),
			Pipes:   "",
		},
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/%s/queries", prefix),
			Handler: handler.Queries,
			Pipes:   "",
		},
		{
			Method:  http.MethodGet,
			Path:    fmt.Sprintf("/%s/params/{elevar}/ble/{ele}", prefix),
			Handler: handler.Params,
			Pipes:   "",
		},
	}
	Routers = append(Routers, r...)
}
