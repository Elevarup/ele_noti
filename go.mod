module gitlab.com/Elevarup/ele_noti

go 1.21

require (
	github.com/gorilla/mux v1.8.0
	github.com/jordan-wright/email v4.0.1-0.20210109023952-943e75fe5223+incompatible
	github.com/twilio/twilio-go v1.11.0
)

require (
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/golang/mock v1.6.0 // indirect
	github.com/ilyakaznacheev/cleanenv v1.5.0 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stianeikeland/go-rpio/v4 v4.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
